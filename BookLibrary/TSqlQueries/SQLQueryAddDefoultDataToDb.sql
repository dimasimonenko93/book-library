﻿USE BookLibrary;

INSERT INTO Writers(FirstName, LastName) 
VALUES
(N'Lion ', N'Tolstoy'),
(N'Taras ', N'Shevchenko'),
(N'Alex ', N'Ferguson'),
(N'Caroline ', N'Vaughan'),
(N'Set ', N'Stevens-Davidovits'),
(N'Alexander ', N'Pushkin'),
(N'Vytold ', N'Fokin')


INSERT INTO Books ([Name], PublicationYear, Genre, [Language], NumberOfPages, Publisher, RecommendedAge, Price, ISBN) 
VALUES
(N'War and Peace ', 2016, N'Roman-epic', N'Russian ', 1120, N'SZKEO', 16, 929, '978-5-9603-0439-9'),
(N'War and Peace ', 2018, N'Roman-epic', N'Russian ', 1120, N'Azbuka, ABC-Atticus', 16, null, '978-5-389-14704-1') ,
(N'War and Peace ', 2016, N'Roman-epic', N'Russian ', 1120, N'Eksmo', 16, null, '978-5-699-93120-0'),
(N'Kobzar ', 2013, N'Book of Poems', N'Ukrainian ', 576, N'School', 12, 839, '978-966-429-192-4'),
(N'Kobzar ', 2013, N'Loads, Lyrics', N'Ukrainian ', 448, N'Masters workshop of creative binding Etalon', 16, 8600, 'KP2512'),
(N'Three lita ', 2018, N'Learns', N'Ukrainian ', 376, N'Libid', null, 225, '978-966-06-0657-9'),
(N'Zapovit ', 2016, N'Zbіrka tvorіv', N'Ukrainsky ', 816, N'Folio', null, 3200, '978-966-03-8231-2'),
(N'Katerina ', 2013, N'Ukrainian literature', N'Ukrainian ', 157, N'Folio', null, 38, '978-966-03-6252-9'),
(N'Gaydamaki ', 2013, N'Ukrainian literature', N'Ukrainian ', 288, N'Folio', null, 44, '978-966-03-6334-2'),
(N'Ruslan i Ludmila ', 2015, N'Russian literature', N'Ukrainsky ', 160, N'Folio', null, 150, '978-966-03-7372-3'),
(N'Alex Ferguson. Autobiography ', 2015, N'Autobiography', N'Russian ', 464, N'AST', 16, 473, '978-966-03-8231-2'),
(N'Michelangelos Diary ', 2016, N'Biography', N'Russian ', 336, N'AST', 12, 369, '978-5-17-106365-8'),
(N'All lie. Search engines, BIG DATA and the Internet know everything about you', 2016, N'IT bestseller', N'Russian ', 384, N'EXMO', 18, 231, '978-5-04- 090836-3')

INSERT INTO WritersBooks(WritersId, BookId) 
VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(3, 11),
(4, 12),
(5, 13),
(6, 10),
(7, 10)

INSERT INTO Copies(BookId, InventoryNumber) 
VALUES
(1, 11),
(1, 12),
(2, 22),
(3, 32),
(4, 41),
(4, 42),
(5, 51),
(5, 52),
(5, 53),
(6, 61),
(7, 71),
(8, 81),
(9, 91),
(9, 92),
(9, 93),
(10, 101)

INSERT INTO PersonsInfo(FirstName, SecondName, Patronymic, Age, Sex, PassportDetails)
VALUES
(N'Valentin ', N'Boyko', N'Ivanovich ',' 1997-11-13 ', N'male', N'AB216658 '),
(N'Stepan ', N'Cherry', N'Yakovlevich ',' 1973-07-10 ', N'male', N'AB118653 '),
(N'Ignatius', N'Sparrow ', N'Radionovich', '1983-06-11', N'male ', N'AB117659'),
(N'Dmitriy ', N'Berezkin', N'Yakovlevich ',' 1977-08-23 ', N'male', N'AB224353 '),
(N'Alexander ', N'Muhich', N'Fyodorovich ',' 1966-10-22 ', N'male', N'AB236868 '),
(N'Stepan ', N'Chayko', N'Vasilevich ',' 1976-12-03 ', N'male', N'AB814643 '),
(N'Fyodor ', N'Shylo', N'Ivanovich ',' 1961-08-17 ', N'male', N'АГ216458 '),
(N'Alisa ', N'Sky', N'Ulyanovna ',' 1961-02-13 ', N'female', N'AA968650 ')

INSERT INTO PhoneNumber(PersonId, Number, IsMain)
VALUES					
(1, 0638884415, 1),
(1, 0675648641, 0),
(2, 0697664754, 1),
(3, 0675668741, 1),
(4, 0697364654, 1),
(5, 0635445671, 1),
(6, 0697634314, 1),
(6, 0635648541, 0),
(7, 0697968774, 1),
(7, 0735998601, 0),
(8, 0690604704, 1),
(8, 0695048541, 0),
(2, 0693634509, 0),
(3, 0734031851, 0)

INSERT INTO EmailAddresses(PersonId, Email, IsMain)
VALUES					
(1, 'VIBoiko@gmail.com', 1),
(1, 'workBoiko@yandex.ru', 0),
(2, 'Yakovlevich76@gmail.com', 1),
(2, 'Yakov1976@gmail.com', 0),
(3, 'Ign83@gmail.com', 1),
(4, 'Ber1977@gmail.com', 1),
(5, 'myxich@gmail.com', 1),
(6, 'xfqrj76@gmail.com', 1),
(7, 'atljhibkj@yandex.ru', 1)


INSERT INTO Readers(PersonId)
VALUES					
(1),
(2),
(3),
(4),
(5),
(6),
(7)

INSERT INTO Positions
VALUES
(N'Librarian'),
(N'cleaner')

INSERT INTO EmployeeInfo(PersonId, Birthplace, ResidenceAddress, MaritalStatus, EducationBackground, IdentificationCode, BankDetails, PositionId)
VALUES
(2, N'Ukraine, Vinnitsa ', N'Keletskaya 16', N'Married ', N'VNTU', N'02449114463678 ', N'260073011442', 1),
(7, N'Ukraine, Kharkov ', N'Zamotyanskaya 33', N'Single ', N'KNAU', N'12479124560071 ', N'345073075342', 1),
(1, N'Ukraine, Vinnitsa ', N'Matrosa Koshki 7', N'Married ', N'TTU', N'52409238863772 ', N'8436477960', 2),
(3, N'Ukraine, Kharkov ', N'Zamotyanskaya 23', N'Single ', N'KNAU', N'12479124560073 ', N'345073075342', 2),
(4, N'Ukraine, Vinnitsa ', N'Matrosa Koshki 7', N'Married ', N'TTU', N'52409238863774 ', N'8436477960', 1),
(5, N'Ukraine, Kharkov ', N'Zamotyanskaya 31', N'Single ', N'KNAU', N'12479124560078 ', N'345073075342', 1)

INSERT INTO History(CopiesId, ReaderId, LibrarianId, BookTaken)
VALUES
(1, 1, 1, '2017-11-13 12:43:10'),
(2, 2, 1, '2017-11-17 11:33:12'),
(3, 3, 1, '2018-12-18 10:13:12'),
(4, 4, 1, '2018-12-18 11:22:42'),
(5, 5, 2, '2018-12-19 12:23:09'),
(6, 6, 2, '2018-12-24 11:31:44'),
(7, 7, 2, '2018-12-26 09:43:23'),
(8, 3, 2, '2019-01-20 12:32:31'),
(9, 4, 1, '2019-02-07 16:45:10'),
(10, 7, 2, '2019-02-13 17:10:03'),
(11, 2, 2, '2019-03-05 16:03:11'),
(12, 5, 1, '2019-03-06 15:46:54'),
(13, 4, 1, '2019-03-09 14:34:41'),
(14, 6, 2, '2019-04-17 14:51:37'),
(15, 6, 2, '2019-04-22 17:53:59'),
(16, 6, 2, '2019-05-11 13:43:44')