﻿USE master;
ALTER DATABASE BookLibrary SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
GO
DROP DATABASE BookLibrary
GO
CREATE DATABASE BookLibrary
GO

USE BookLibrary

CREATE TABLE Books
(
 Id INT PRIMARY KEY IDENTITY,
 [Name] NVARCHAR(MAX) CHECK([Name] !=''),
 PublicationYear SMALLINT,
 Genre NVARCHAR(MAX),
 [Language] NVARCHAR(30),
 NumberOfPages SMALLINT CHECK(NumberOfPages > 0),
 Publisher NVARCHAR(MAX),
 RecommendedAge TINYINT CHECK(RecommendedAge > 0 AND RecommendedAge < 150 AND RecommendedAge = NULL),
 Price MONEY CHECK(Price >= 0 AND Price = NULL),
 ISBN NVARCHAR(MAX) NOT NULL
)

CREATE TABLE Writers
(
 Id INT PRIMARY KEY IDENTITY,
 FirstName NVARCHAR(MAX),
 LastName NVARCHAR(MAX),
)


CREATE TABLE WritersBooks
(
 WritersId INT REFERENCES Writers (Id),
  FOREIGN KEY (WritersId) REFERENCES Writers(Id) ON DELETE CASCADE,
 BookId INT REFERENCES Books (Id),
  FOREIGN KEY (BookId) REFERENCES Books(Id) ON DELETE CASCADE,
)

CREATE TABLE Copies
(
 Id INT PRIMARY KEY IDENTITY,
 BookId INT REFERENCES Books (Id),
  FOREIGN KEY (BookId) REFERENCES Books(Id) ON DELETE CASCADE,
 InventoryNumber NVARCHAR(MAX)
)

CREATE TABLE PersonsInfo
(
 Id INT PRIMARY KEY IDENTITY,
 FirstName NVARCHAR(20),
 SecondName NVARCHAR(20),
 Patronymic NVARCHAR(20),
 Age SMALLDATETIME CHECK(Age >= 0),
 Sex NVARCHAR(20),
 PassportDetails NVARCHAR(20) UNIQUE
)

CREATE TABLE PhoneNumber
(
 PersonId INT,
  FOREIGN KEY (PersonId) REFERENCES PersonsInfo(Id) ON DELETE CASCADE,
 Number INT UNIQUE,
 IsMain BIT
)

CREATE TABLE EmailAddresses
(
 PersonId INT,
  FOREIGN KEY (PersonId) REFERENCES PersonsInfo(Id) ON DELETE CASCADE,
 Email NVARCHAR(254) UNIQUE,
 IsMain BIT
)

CREATE TABLE Readers
(
 Id INT PRIMARY KEY IDENTITY,
 PersonId INT,
  FOREIGN KEY (PersonId) REFERENCES PersonsInfo(Id) ON DELETE CASCADE,
)


CREATE TABLE Positions
(
 Id INT PRIMARY KEY IDENTITY,
 [Name] NVARCHAR(MAX),
)
insert into Positions values ('Librarian');

CREATE TABLE EmployeeInfo
(
 Id INT PRIMARY KEY IDENTITY,
 PersonId INT,
  FOREIGN KEY (PersonId) REFERENCES PersonsInfo(Id) ON DELETE CASCADE,
 Birthplace NVARCHAR(MAX),
 ResidenceAddress NVARCHAR(MAX),
 MaritalStatus NVARCHAR(20),
 EducationBackground NVARCHAR(MAX),
 IdentificationCode NVARCHAR(50) UNIQUE,
 BankDetails NVARCHAR(MAX),
 PositionId INT,
  FOREIGN KEY (PositionId) REFERENCES Positions(Id), --,?
)

CREATE TABLE Librarians
(
 Id INT PRIMARY KEY IDENTITY,
 EmployeeId INT,
  FOREIGN KEY (EmployeeId) REFERENCES EmployeeInfo(Id),
)

CREATE TABLE History
(
 Id INT PRIMARY KEY IDENTITY,
 CopiesId INT,
  FOREIGN KEY (CopiesId) REFERENCES Copies(Id),
 ReaderId INT,
  FOREIGN KEY (ReaderId) REFERENCES Readers(Id),
 LibrarianId INT,
  FOREIGN KEY (LibrarianId) REFERENCES Librarians(Id),
 BookTaken SMALLDATETIME,
 TimeToReturn SMALLDATETIME,
 BookReturned SMALLDATETIME
)
GO

CREATE TRIGGER EmployeeInfo_Insert
ON EmployeeInfo
AFTER INSERT
AS
insert into Librarians (EmployeeId)
 select i.Id from inserted i
  left join Librarians l on l.EmployeeId = i.Id
   where i.PositionId = 1 and l.Id is null
go

CREATE TRIGGER EmployeeInfo_Delete
ON EmployeeInfo
AFTER DELETE
AS
DELETE l FROM Librarians l
 join deleted d on d.Id = l.EmployeeId
 left join History as h on h.LibrarianId = d.Id
  where h.Id is null and l.EmployeeId is not null
go

 create proc InsertToLibrarian
(
 @EmployeeId int
)as
begin
insert into Librarians (EmployeeId)
 values(@EmployeeId)
end 
go

create proc DeleteFromLibrarian
(
 @EmployeeId int
)as
begin
delete l from Librarians l
 where l.EmployeeId = @EmployeeId
end 
go

create proc InsertOrDeleteLibrarian
(
 @EmployeeId int,
 @PositionId int
)as
begin
 if @PositionId = 1
  exec InsertToLibrarian @EmployeeId
 else 
  exec DeleteFromLibrarian @EmployeeId
end 
go

CREATE TRIGGER EmployeeInfo_Update
on EmployeeInfo	
after UPDATE
as
create table #inserted(EmployeeId int, PositionId int)
insert into #inserted (EmployeeId, PositionId) 
select i.Id, i.PositionId from inserted i, deleted d
 where i.Id = d.Id
  and ((d.PositionId = 1 and i.PositionId != 1) or (d.PositionId != 1 and i.PositionId = 1))
declare curForUTrigger cursor for 
 select EmployeeId, PositionId
 from #inserted

declare @EmployeeId int
declare @PositionId int

open curForUTrigger
fetch next from curForUTrigger into @EmployeeId, @PositionId
while @@FETCH_STATUS = 0
begin
 exec InsertOrDeleteLibrarian @EmployeeId, @PositionId
 fetch next from curForUTrigger into @EmployeeId, @PositionId
end
drop table #inserted
close curForUTrigger
deallocate curForUTrigger
go