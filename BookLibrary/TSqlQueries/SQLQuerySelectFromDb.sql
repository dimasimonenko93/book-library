use BookLibrary;

SELECT SecondName, FirstName, DATEDIFF(year, Age, GETDATE()) 
 FROM PersonsInfo 
  WHERE datediff(yy, Age, getdate()) > 55;

SELECT PersonsInfo.FirstName, PersonsInfo.SecondName, PhoneNumber.Number 
 FROM PersonsInfo, PhoneNumber, Readers 
  WHERE datediff(yy, Age, getdate()) < 30 
   AND PersonsInfo.Id = Readers.PersonId 
   AND PhoneNumber.PersonId = PersonsInfo.Id 
   AND PhoneNumber.IsMain = 1;

SELECT PersonsInfo.FirstName, PersonsInfo.SecondName, Copies.InventoryNumber, Writers.FirstName, Writers.LastName, Books.[Name], Books.Price 
 FROM History, PersonsInfo, Readers, Copies, WritersBooks
 JOIN Books ON Books.Id = WritersBooks.BookId
 JOIN Writers ON Writers.Id = WritersBooks.WritersId
  WHERE History.BookReturned IS NULL 
   AND History.CopiesId = Copies.Id 
   AND Books.Id = Copies.BookId 
   AND Readers.Id = History.ReaderId 
   AND PersonsInfo.Id = Readers.Id

SELECT Writers.FirstName, Writers.LastName, Books.[Name], Books.PublicationYear
 FROM WritersBooks
 JOIN Books ON Books.Id = WritersBooks.BookId
 JOIN Writers ON Writers.Id = WritersBooks.WritersId
  WHERE Books.Genre LIKE '%�����%';

SELECT Writers.FirstName, Writers.LastName, Books.[Name], COUNT(*) AS CopiesCount
 FROM Copies, WritersBooks
 JOIN Books ON Books.Id = WritersBooks.BookId
 JOIN Writers ON Writers.Id = WritersBooks.WritersId
  WHERE Books.Id = Copies.BookId
  GROUP BY Writers.FirstName, Writers.LastName, Books.[Name]
  HAVING COUNT(*) > 1
  ORDER BY CopiesCount

SELECT Writers.FirstName, Writers.LastName, COUNT(WritersBooks.WritersId) AS BooksCount
 FROM  WritersBooks
 JOIN Books ON Books.Id = WritersBooks.BookId
 JOIN Writers ON Writers.Id = WritersBooks.WritersId
  GROUP BY Writers.FirstName, Writers.LastName 
  HAVING COUNT(WritersBooks.WritersId) > 1

select top 1 count(History.LibrarianId) as LibrarianIdCount, History.LibrarianId
from History
 group by History.LibrarianId
  order by LibrarianIdCount desc

select top 1 count(History.ReaderId) as ReaderIdCount, History.ReaderId
from History
 group by History.ReaderId
  order by ReaderIdCount desc

select top 1 count(History.ReaderId) as ReaderIdCount, History.ReaderId
 from History
  WHERE History.BookTaken > DATEADD(year,-1,GETDATE())
   group by History.ReaderId
    order by ReaderIdCount desc