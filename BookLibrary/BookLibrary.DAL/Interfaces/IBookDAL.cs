﻿using BookLibrary.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.DAL.Interfaces
{
    public interface IBookDAL
    {
        List<Book> GetAllBooks();
        List<Book> GetCopiesOfBook(int bookId);
        List<Book> FindBooksByName(string bookName);
        List<Book> FindBooksByWriters(string writerName);
        List<Book> FindBooksByInventoryNumber(string inventoryNumber);
        void DeleteBook(int bookId);
        int CreateOrUpdateBook(Book item);
        void ReturnBook(int bookCopyId);
    }
}
