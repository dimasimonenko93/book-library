﻿using BookLibrary.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.DAL.Interfaces
{
    public interface IHistoryDAL
    {
        void GiveBook(int bookCopyId, int readerId, int librarianId, DateTime timeToReturnBook);
        List<History> GetReaderHistory(int readerId);
    }
}
