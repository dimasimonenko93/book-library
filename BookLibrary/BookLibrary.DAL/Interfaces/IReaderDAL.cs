﻿using BookLibrary.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.DAL.Interfaces
{
    public interface IReaderDAL
    {
        List<Reader> GetAllReaders();
        List<Reader> FindReadersByName(string readerName);
        void DeleteReader(int readerId);
        int CreateOrUpdateReader(Reader item);
        List<Reader> GetDebtors();
    }
}
