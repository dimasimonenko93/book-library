﻿using System.Collections.Generic;

namespace BookLibrary.Models.Entities
{
    public class Reader : IModel
    {
        public int    Id              { get; set; }
        public string FirstName       { get; set; }
        public string SecondName      { get; set; }
        public string Patronymic      { get; set; }
        public int    Age             { get; set; }
        public string Sex             { get; set; }
        public string PassportDetails { get; set; }
    }
}
