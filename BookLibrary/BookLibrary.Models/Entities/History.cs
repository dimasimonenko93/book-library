﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Models.Entities
{
    public class History : IModel
    {
        public int Id                { get; set; }
        public int CopiesId          { get; set; }
        public int ReaderId          { get; set; }
        public int LibrarianId       { get; set; }
        public DateTime BookTaken    { get; set; }
        public DateTime TimeToReturn { get; set; }
        public DateTime BookReturned { get; set; }

    }
}
