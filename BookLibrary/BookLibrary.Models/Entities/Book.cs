﻿namespace BookLibrary.Models.Entities
{
    public class Book : IModel
    {
        public int    Id              { get; set; }
        public string Name            { get; set; }
        public int    PublicationYear { get; set; }
        public string Genre           { get; set; }
        public string Language        { get; set; }
        public int    NumberOfPages   { get; set; }
        public string Publisher       { get; set; }
        public int    RecommendedAge  { get; set; }
        public float   Prise          { get; set; }
        public string   ISBN          { get; set; }
    }
}
